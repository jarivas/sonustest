const Sonus = require('sonus');

const speech = require('@google-cloud/speech')({
    projectId: 'streaming-speech-sample',
    keyFilename: ROOT_DIR + 'keyfile.json'
  })
  
  const hotwords = [{ file: 'resources/sonus.pmdl', hotword: 'sonus' }];
  const language = "en-US";
  const recordProgram = "arecord";
  const device = "hw:0,1";
  const sonus = Sonus.init({ hotwords, language, recordProgram, device }, speech)
  
  Sonus.start(sonus);
  sonus.on('hotword', (index, keyword) => console.log("!" + keyword))
  sonus.on('partial-result', result => console.log("Partial", result))
  
  sonus.on('final-result', result => {
    console.log("Final", result)
    if (result.includes("stop")) {
      Sonus.stop()
    }
  });